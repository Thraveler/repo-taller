let clientes;

function getCustomers() {
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers"
  let http = new XMLHttpRequest();

  http.onreadystatechange = function() {
    if(this.readyState == 4 || this.status == 200) {
      clientes = JSON.parse(http.responseText).value;
      procesarClientes();
    }
  };

  http.open("GET", url, true);
  http.send();
}

function procesarClientes() {

  var divTabla = document.getElementById('divTablaClientes');
  var tabla = document.createElement('table');
  var tbody = document.createElement('tbody');

  tabla.classList.add('table');
  tabla.classList.add('table-hover');

  for(var i = 0; i < clientes.length; i++) {
    var nuevaFila = document.createElement('tr');
    var columnaNombre = document.createElement('td');
    columnaNombre.innerText = clientes[i].ContactName;
    
    var columnaCiudad = document.createElement('td');
    columnaCiudad.innerText = clientes[i].City;
    
    var columnaPais = document.createElement('td');
    var banderaPais = document.createElement('img');
    if(clientes[i].Country === 'UK') {
      banderaPais.setAttribute("src", `https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png`);
    } else {
      banderaPais.setAttribute("src", `https://www.countries-ofthe-world.com/flags-normal/flag-of-${clientes[i].Country}.png`);
    }
    banderaPais.classList.add('flag');
    columnaPais.appendChild(banderaPais);
    // columnaPais.innerText = `<img src="https://www.countries-ofthe-world.com/flags-normal/flag-of-${clientes[i].Country}.png"/>`

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}