var productos;

function getProducts() {
  var url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Products'
  var http = new XMLHttpRequest();

  http.onreadystatechange = function() {
    if(this.readyState == 4 || this.status == 200) {
      productos = JSON.parse(http.responseText).value;
      procesarProductos();
    }
  };

  http.open("GET", url, true);
  http.send();
}

function procesarProductos() {

  var divTabla = document.getElementById('divTablaProductos');
  var tabla = document.createElement('table');
  var tbody = document.createElement('tbody');

  tabla.classList.add('table');
  tabla.classList.add('table-striped');

  for(var i = 0; i < productos.length; i++) {
    var nuevaFila = document.createElement('tr');
    var columnaNombre = document.createElement('td');
    columnaNombre.innerText = productos[i].ProductName;
    
    var columnaPrecio = document.createElement('td');
    columnaPrecio.innerText = productos[i].UnitPrice;
    
    var columnaStock = document.createElement('td');
    columnaStock.innerText = productos[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}